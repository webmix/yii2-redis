<?php
/**
 * User: gg
 * Date: 17.06.2015
 * Time: 17:59
 */

namespace webmix\redis;

use Yii;
use yii\base\Component;

class Channel extends Component
{
    const EVENT_RECEIVE = 'RECEIVE';
    /**
     * The name of the redis channel
     * @var string
     */
    public $name;
    protected $client;

    public function __construct($name, \Redis $client)
    {
        $this->name = $name;
        $this->client = $client;
    }

    /**
     * Subscribes to the channel
     * @return  $this subscribed to the channel
     */
    public function subscribe()
    {
        $this->client->subscribe([$this->name], [$this, 'receiveMessage']);
        return $this;
    }

    /**
     * Publishes a message to the channel
     * @param string $message The message to publish
     * @return integer the number of clients that received the message
     */
    public function publish($message)
    {
        return $this->client->publish($this->name, $message);
    }

    /**
     * Receives a message from a subscribed channel
     * @param \Redis $redis the redis client instance
     * @param string $channel the name of the channel
     * @param string $message the message content
     */
    public function receiveMessage($redis, $channel, $message)
    {
        $event = new REvent();
        $event->message = $message;
        $this->trigger(self::EVENT_RECEIVE, $event);
    }
}